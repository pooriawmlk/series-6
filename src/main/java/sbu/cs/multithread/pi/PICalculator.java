package sbu.cs.multithread.pi;

public class PICalculator {

    public String calculate(int floatingPoint) {
        Calculate calculate = new Calculate(floatingPoint+2);

        try {
            StringBuilder ans = new StringBuilder(calculate.compute().toString());
            return ans.deleteCharAt(ans.length()-1).toString();
        }
        catch (InterruptedException exception) {
            return exception.getMessage();
        }

    }
}
