package sbu.cs.exception;

import java.util.List;

public class Reader {

    public void readTwitterCommands(List<String> args) throws ApException
    {
        for (String arg : args)
            if(Util.getNotImplementedCommands().contains(arg))
                throw new NotImplementedCommandException();


        for (String arg : args)
            if(!Util.getImplementedCommands().contains(arg))
                throw new UnrecognizedCommandException();
    }

    public void read(String...args) throws BadInputException
    {
        String[] input = args;

        for (int i = 1; i < args.length; i += 2)
        {
            try
            {
                Integer.parseInt(input[i]);
            }
            catch (NumberFormatException exception)
            {
                throw new BadInputException();
            }
        }

    }
}
